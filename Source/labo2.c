#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

//source: https://jkorpela.fi/forms/cgic.html#:~:text=A-,relatively%20simple,-C%20program%20for

#define MAXLEN 80
#define EXTRA 5
/* 4 for field name "data", 1 for "=" */
#define MAXINPUT MAXLEN+EXTRA+2
#define DATAFILE "/var/www/html/info.json"

void unencode(char *src, char *last, char *dest)
{
 for(; src != last; src++, dest++)
   if(*src == '+')
     *dest = ' ';
   else if(*src == '%') {
     int code;
     if(sscanf(src+1, "%2x", &code) != 1) code = '?';
     *dest = code;
     src +=2; }     
   else
     *dest = *src;
 *dest = '\n';
 *++dest = '\0';
}
void removeNewline(char data[])
{
  // om nieuw lines te verwijderen
  char str[40];
  unsigned int len = sprintf(str, data);
  str[len - 1] = 0;
  sprintf(data, str);
}

void remove_spaces(char* s) {
    char* d = s;
    do {
        while (*d == ' ') {
            ++d;
        }
    } while (*s++ = *d++);
}


int main(void)
{
  char *lenstr;
  char input[MAXINPUT], data[MAXINPUT];
  long len;
  char *lenstrcookie;
  char myurl[30] = "";
  
  FILE *datafile;
  FILE *myfile;
  printf("%s%c%c\n","Content-Type:text/html;charset=iso-8859-1",13,10);
  printf("<TITLE>Response</TITLE>\n");
  lenstr = getenv("CONTENT_LENGTH");

  if (lenstr == NULL || sscanf(lenstr, "%ld", &len) != 1 || len > MAXLEN)
    {
        printf("<P>Error in invocation - wrong FORM probably.");
    }


  sscanf(lenstr,"%ld",&len)!=1;
  fgets(input, len+1, stdin);
  unencode(input+EXTRA, input+len, data);


  
  datafile = fopen("/var/www/html/info.json", "r+");
  if (datafile != NULL)
  {

    /* Define temporary variables */
    time_t current_time;
    struct tm *local_time;

    /* Retrieve the current time */
    current_time = time(NULL);
    /* Get the local time using the current time */
    local_time = localtime(&current_time);
    char datum[40] = "";
    sprintf(datum, "%s", asctime(local_time));
    removeNewline(data);
    removeNewline(datum);
    char dataRow[100] = "";
    sprintf(dataRow, ",{\"commentaar\":\"%s\",\"Timestamp\":\"%s\"}]", data, datum);
    printf("<p>Commentaar: %s</p>", data);
    printf("<p>Timestamp: %s</p>", asctime(local_time));
    fseek(datafile, -1, SEEK_END);
    fputs(dataRow, datafile);
    printf("<P>Thank you! Your contribution has been stored in the json file.");
  }
  
  fclose(datafile);

  return 0;
}