# How to install

Files that are editable are under Source directory

The CMakeLists.txt file should put these in the build directory:

In the build directory:

cmake ../Source
make
sudo /LABO2

Move the LABO2 executable:

#sudo mv LABO2 /usr/lib/cgi-bin/LABO2
#sudo chmod a+rx /usr/lib/cgi-bin/LABO2

Move the index.html-file to the correct place on the pi:

#sudo cp ./index.html /var/www/html/index.html
#sudo chmod a+rx /var/www/html/index.html